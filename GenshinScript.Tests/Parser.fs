module Genshin.Tests.Parser

open FParsec

open Xunit
open FsUnit.CustomMatchers
open FsUnit.Xunit

open Genshin
open Genshin.Lang
open Ast
open Parser.Internal

let parsedFrom (parser: Parser<'a, unit>) source (expected: 'a) =
    run (parser .>> eof) source
    |> Util.toResult
    |> should equal (Result<'a, string>.Ok expected)

let failsOn source (parser: Parser<'a, unit>) =
    run (parser .>> eof) source
    |> Util.toResult
    |> should be (ofCase <@ Result<'a, string>.Error @>)

[<Fact>]
let ``注释为C++风格`` () =
    () |> parsedFrom Helpers.spaces "  //  "
    () |> parsedFrom Helpers.spaces "/*...*/"

[<Fact>]
let ``标识符包括字母, 数字和下划线, 不可以数字开头.`` () =
    "abc" |> parsedFrom Elements.ident "abc"
    Elements.ident |> failsOn "1aaa"
    Elements.ident |> failsOn "ab?c"
    "我是中文" |> parsedFrom Elements.ident "我是中文"

[<Fact>]
let ``括号包围, 逗号分隔的标识符组成形参列表, 匿名函数的签名有一个胖箭头(=>)和可选的函数名.`` () =
    [ "a"; "b" ] |> parsedFrom Elements.parameters "( a, b )"
    ([ "a" ], None) |> parsedFrom Elements.lambdaParameters "a=> "
    ([ "a"; "b" ], Some "c") |> parsedFrom Elements.lambdaParameters "(a,b) c =>"

    ([], None) |> parsedFrom Elements.lambdaParameters "() => "

[<Fact>]
let ``字符中可以有转义序列和unicode代码点, 引号字符串是双引号包围的若干字符.`` () =
    ' ' |> parsedFrom Elements.ochar @"\u0020"
    '\n' |> parsedFrom Elements.ochar @"\n"
    Elements.ochar |> failsOn "\""
    "你好hello" |> parsedFrom Elements.quotedString "\"你好hello\""

[<Fact>]
let ``简单的非数值字面量.`` () =
    Constant null |> parsedFrom Factor.oNull "null"
    Constant true |> parsedFrom Factor.oBool "真"
    Constant 'a' |> parsedFrom Factor.oChar "'a'"
    Constant "a\tc" |> parsedFrom Factor.oString "\"a\\tc\""

[<Fact>]
let ``数值类型由后缀表示类型.`` () =
    Constant 1 |> parsedFrom Factor.oInt "1"
    Constant 1u |> parsedFrom Factor.oInt "1u"
    Constant 1I |> parsedFrom Factor.oInt "1I"
    Factor.oInt |> failsOn "99999s"
    Factor.oInt |> failsOn "000"
    Constant 1.0 |> parsedFrom Factor.oFloat "1.0"
    Constant 1.0f |> parsedFrom Factor.oFloat "1.0f"
    Constant 1.0M |> parsedFrom Factor.oFloat "1.0M"

[<Fact>]
let ``因数`` () =
    Constant 1 |> parsedFrom Factor.factor "1"
    Constant 1.0 |> parsedFrom Factor.factor "1.0"
    UnaryOp(Negate, (Name "foo")) |> parsedFrom Factor.factor "-foo"
    Attribute(Name "foo", "bar") |> parsedFrom Factor.factor "foo.bar"
    Call(Name "f", [ Name "x" ]) |> parsedFrom Factor.factor "f ( x )"

[<Fact>]
let ``中缀运算`` () =
    BinOp(Constant 1, Mul, UnaryOp(Negate, (Constant 1)))
    |> parsedFrom BinOp.term "1*-1"

    BinOp(Constant 1, Add, Constant 1) |> parsedFrom BinOp.arithExpr "1 + 1"

    BinOp(Constant 1, Add, BinOp(Constant 2, Mul, Constant 3))
    |> parsedFrom BinOp.arithExpr "1 + 2*3"

    BinOp.arithExpr |> failsOn "1**1"

    BoolOp(And, [ Constant true; Constant false ])
    |> parsedFrom BinOp.andTest "true && false"

    BoolOp(Or, [ Name "a"; BoolOp(And, [ Name "b"; Name "c"; BinOp(Name "d", Lt, Name "e") ]) ])
    |> parsedFrom BinOp.orTest "a || b && c && d < e"

    Assign(NameTarget "a", Assign(AttributeTarget(Name "b", "c"), Constant 1))
    |> parsedFrom BinOp.expr "a = b. c =1"

    AugAssign(SubscriptTarget(Name "a", [ Name "b" ]), Add, Constant "hello")
    |> parsedFrom BinOp.expr "a[b]+=\"hello\""

[<Fact>]
let ``数组和对象由方括号, 花括号包围`` () =
    Array [ Constant 1; Constant 2 ] |> parsedFrom Factor.oArray "[1, 2]"

    Object
        [ AttributeEntry("a", Constant 1)
          IndexEntry(Constant "b", Constant 2)
          FunctionEntry("f", [ "c" ], list<Statement>.Empty) ]
    |> parsedFrom Factor.oObject "{a:1, [\"b\"] : 2, f( c ){} }"

[<Fact>]
let ``匿名函数可以只包含一个表达式, 也可以有多个语句`` () =
    SimpleLambda([], None, Constant 1) |> parsedFrom Expr.oLambda "() => 1"

    SimpleLambda([ "x" ], Some "f", Constant "bye")
    |> parsedFrom Expr.oLambda "(x) f => \"bye\""

[<Fact>]
let ```三元运算`` () =
    IfExpr(BinOp(Constant 1, Eq, Constant 1), Constant false, Constant true)
    |> parsedFrom ForwardReference.oExpr "1 == 1?false  :true"

[<Fact>]
let ``简单语句`` () =
    Continue |> parsedFrom Statement.oContinue "continue;"
    Return None |> parsedFrom Statement.oReturn "return   ;"
    Throw(Constant null) |> parsedFrom Statement.oThrow "throw null;"

[<Fact>]
let ``let与if`` () =
    Let [ LetProperties([ "a"; "b" ], Object [ AttributeEntry("a", Constant 1) ]) ]
    |> parsedFrom Statement.oLet "let { a, b } = { a: 1 };"

    Let [ LetValue("a", Some <| Name "b"); LetValue("c", None) ]
    |> parsedFrom Statement.oLet "let a = b, c;"

    If(Constant true, [ Break ], None)
    |> parsedFrom Statement.oIf "if (true) { break; }"

    If(Constant false, [ Continue ], Some [ Call(Name "say", []) |> Expr ])
    |> parsedFrom Statement.oIf "if(false)continue; else {say();}"

[<Fact>]
let ``循环结构`` () =
    While(Constant true, [ Pass ]) |> parsedFrom Statement.oWhile "while(true);"

    ForEach("i", Name "iterable", [ Expr(Call(Name "say", [ Name "i" ])) ])
    |> parsedFrom Statement.oForEach "foreach (i in iterable) say(i);"

    ForEach("某", Name "可迭代对象", [ Expr(Call(Name "说", [ Name "某" ])) ])
    |> parsedFrom Statement.oForEach "对于 （可迭代对象 中的 某）｛说（某）；｝"

    For(None, None, None, [ Pass ]) |> parsedFrom Statement.oFor "for(;;);"

    For(
        Assign(NameTarget "i", Constant 0) |> ExprInit |> Some,
        None,
        AugAssign(NameTarget "i", Add, Constant 1) |> Some,
        [ Break ]
    )
    |> parsedFrom Statement.oFor "for(i=0; ; i+=1) break;"

    For(
        [ LetValue("i", Some <| Constant 0) ] |> LetInit |> Some,
        BinOp(Name "i", Lt, Constant 10) |> Some,
        None,
        [ Continue ]
    )
    |> parsedFrom Statement.oFor "for ( let i = 0; i < 10; ) {continue;}"

[<Fact>]
let ``函数定义`` () =
    FunctionDef("add1", [ "x" ], [ Return(Some(BinOp(Name "x", Add, Constant 1))) ])
    |> parsedFrom Statement.oFunctionDef "fun add1(x) { return x + 1; }"

[<Fact>]
let ``异常处理`` () =
    Try([], Some <| Handler(None, [ Pass ]))
    |> parsedFrom Statement.oTry "try {} catch ;"

    Try([ Pass ], Some <| Handler(Some "exc", []))
    |> parsedFrom Statement.oTry "try; catch(exc ){ }"

[<Fact>]
let ``导出`` () =
    Export
        [ ExportItem("predefined", None)
          ExportItem("withvalue", Some <| Constant null) ]
    |> parsedFrom TopLevel.oExport "export predefined, withvalue= null;"

    ExportFunction("f", [ "x" ], [ Name "x" |> Some |> Return ])
    |> parsedFrom TopLevel.oExportFunction "export f(x) { return x;}"

[<Fact>]
let ``导入`` () =
    Import(ImportAll, Some "某", "库路径")
    |> parsedFrom TopLevel.oImport "从 \"库路径\" 导入 * 为 某;"

    Import(ImportNames [ "a"; "b" ], None, "path/to/lib")
    |> parsedFrom TopLevel.oImport "import {a, b }from \"path/to/lib\";"
