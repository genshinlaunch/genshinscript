module Genshin.Tests.Compiler

open System.Linq.Expressions

open Xunit
open FsUnit.Xunit

open Genshin
open Genshin.Lang
open Ast
open Compiler

let compileExprFlip = Util.flip compileExpr
let compileStmtFlip = Util.flip compileStatement
let compileTopLevelFlip = Util.flip compileTopLevel

let hasCode code (state: State) =
    state.Code |> List.rev |> should equal code

let both f g x =
    f x
    g x

let withSymbols symbols =
    let map = Seq.zip symbols (Seq.initInfinite id) |> Map.ofSeq

    { initState with
        SymbolTable = [ map ]
        SymbolsCount = List.length symbols }

[<Fact>]
let ``编译简单的表达式`` () =
    Constant null |> compileExprFlip initState |> hasCode [ LoadConst null ]

    Call(Name "say", [ Constant "hello, world" ])
    |> compileExprFlip (withSymbols [ "say" ])
    |> hasCode [ LoadVar(0, 0); LoadConst "hello, world"; FunctionCall 1 ]

    Attribute(Name "a", "b")
    |> compileExprFlip (withSymbols [ "a" ])
    |> hasCode [ LoadVar(0, 0); LoadAttr "b" ]

    Subscript(Subscript(Name "a", [ Name "b" ]), [ Name "c" ])
    |> compileExprFlip (withSymbols [ "a"; "b"; "c" ])
    |> hasCode [ LoadVar(0, 0); LoadVar(1, 0); LoadSubscr 1; LoadVar(2, 0); LoadSubscr 1 ]

[<Fact>]
let ``中缀运算`` () =
    BinOp(Constant 1, Add, Constant 2)
    |> compileExprFlip initState
    |> hasCode [ LoadConst 1; LoadConst 2; BinaryOperation ExpressionType.Add ]

    UnaryOp(UAdd, Constant 1)
    |> compileExprFlip initState
    |> hasCode [ LoadConst 1; UnaryOperation ExpressionType.UnaryPlus ]

    BoolOp(And, [ Constant true; UnaryOp(Not, Constant false); Constant true ])
    |> compileExprFlip initState
    |> hasCode
        [ LoadConst true
          JumpIfFalseOrPop 5
          LoadConst false
          UnaryOperation ExpressionType.Not
          JumpIfFalseOrPop 2
          LoadConst true ]

[<Fact>]
let ``三元运算符和if-else`` () =
    IfExpr(Constant true, Constant 1, Constant 2)
    |> compileExprFlip initState
    |> hasCode [ LoadConst true; PopJumpIfFalse 3; LoadConst 1; Jump 2; LoadConst 2 ]

    If(Constant 0, [ Expr(Constant 1) ], [ 2..4 ] |> List.map (Expr << Constant << box) |> Some)
    |> compileStmtFlip initState
    |> hasCode
        [ LoadConst 0
          PopJumpIfFalse 4
          LoadConst 1
          Pop
          Jump 7
          LoadConst 2
          Pop
          LoadConst 3
          Pop
          LoadConst 4
          Pop ]

    If(Constant false, [ Expr(Constant 1) ], None)
    |> compileStmtFlip initState
    |> hasCode [ LoadConst false; PopJumpIfFalse 4; LoadConst 1; Pop; Nop ]

[<Fact>]
let ``集合字面量`` () =
    Array [ Constant 1 ]
    |> compileExprFlip initState
    |> hasCode [ LoadConst 1; MakeArray 1 ]

    Object
        [ AttributeEntry("a", Constant 1)
          IndexEntry(Constant 2, Constant 3)
          FunctionEntry("f", [], []) ]
    |> compileExprFlip initState
    |> hasCode
        [ LoadConst "a"
          LoadConst 1
          LoadConst 2
          LoadConst 3
          LoadConst "f"
          MakeFunction(Some "f", 0, 0, [| LoadConst null; ReturnValue |])
          MakeObject 3 ]

[<Fact>]
let ``赋值和增强赋值`` () =
    Assign(NameTarget "a", Constant 1)
    |> compileExprFlip (withSymbols [ "a" ])
    |> hasCode [ LoadConst 1; StoreVar(0, 0) ]

    Assign(SubscriptTarget(Name "a", [ Constant 0 ]), Constant 1)
    |> compileExprFlip (withSymbols [ "a" ])
    |> hasCode [ LoadVar(0, 0); LoadConst 0; LoadConst 1; StoreSubscr 1 ]

    AugAssign(AttributeTarget(Name "a", "b"), Add, Constant 1)
    |> compileExprFlip (withSymbols [ "a" ])
    |> hasCode
        [ LoadVar(0, 0)
          Copy
          LoadAttr "b"
          LoadConst 1
          BinaryOperation ExpressionType.Add
          StoreAttr "b" ]

[<Fact>]
let ``匿名函数可以只有一个表达式作为函数体, 允许可选的名字`` () =
    Lambda([], Some "f", [ Expr(Constant 1) ])
    |> compileExprFlip initState
    |> hasCode [ MakeFunction(Some "f", 0, 0, [| LoadConst 1; Pop; LoadConst null; ReturnValue |]) ]

    SimpleLambda([ "a" ], None, BinOp(Name "a", Add, Constant 1))
    |> compileExprFlip initState
    |> hasCode
        [ MakeFunction(
              None,
              1,
              1,
              [| LoadVar(0, 0)
                 LoadConst 1
                 BinaryOperation ExpressionType.Add
                 ReturnValue |]
          ) ]

[<Fact>]
let ``while循环与break, continue语句`` () =
    While(Constant true, [ Break; Expr(Constant 1) ])
    |> compileStmtFlip initState
    |> hasCode [ Jump 4; LoadConst 1; Pop; Jump -3 ]

    While(Name "a", [ Continue ])
    |> compileStmtFlip (withSymbols [ "a" ])
    |> hasCode [ LoadVar(0, 0); PopJumpIfFalse 3; Jump -2; Jump -3 ]

[<Fact>]
let ``for和foreach循环`` () =
    For(
        Some <| LetInit [ LetValue("i", Some <| Constant 0) ],
        Some <| BinOp(Name "i", Lt, Constant 10),
        None,
        [ Expr <| Call(Name "say", [ Name "i" ])
          Expr <| AugAssign(NameTarget "i", Add, Constant 1) ]
    )
    |> compileStmtFlip (withSymbols [ "say" ])
    |> hasCode
        [ LoadConst 0
          StoreVar(1, 0)
          Pop
          LoadVar(1, 0)
          LoadConst 10
          BinaryOperation ExpressionType.LessThan
          PopJumpIfFalse 11
          LoadVar(0, 0)
          LoadVar(1, 0)
          FunctionCall 1
          Pop
          LoadVar(1, 0)
          LoadConst 1
          BinaryOperation ExpressionType.Add
          StoreVar(1, 0)
          Pop
          Jump -13 ]

    For(
        Some <| LetInit [ LetValue("i", Some <| Constant 0) ],
        Some <| BinOp(Name "i", Lt, Constant 10),
        Some <| AugAssign(NameTarget "i", Add, Constant 1),
        [ Expr <| Call(Name "say", [ Name "i" ]) ]
    )
    |> compileStmtFlip (withSymbols [ "say" ])
    |> hasCode
        [ LoadConst 0
          StoreVar(1, 0)
          Pop
          Jump 6
          LoadVar(1, 0)
          LoadConst 1
          BinaryOperation ExpressionType.Add
          StoreVar(1, 0)
          Pop
          LoadVar(1, 0)
          LoadConst 10
          BinaryOperation ExpressionType.LessThan
          PopJumpIfFalse 6
          LoadVar(0, 0)
          LoadVar(1, 0)
          FunctionCall 1
          Pop
          Jump -13 ]

    ForEach("i", Name "a", [ Expr(Call(Name "say", [ Name "i" ])) ])
    |> compileStmtFlip (withSymbols [ "say"; "a" ])
    |> hasCode
        [ LoadVar(1, 0)
          GetIter
          ForIter 8
          StoreVar(2, 0)
          Pop
          LoadVar(0, 0)
          LoadVar(2, 0)
          FunctionCall 1
          Pop
          Jump -7 ]

[<Fact>]
let ``异常处理`` () =
    Throw(Constant "error")
    |> compileStmtFlip initState
    |> hasCode [ LoadConst "error"; ThrowException ]

    Try([ Throw(Constant("error")) ], Some <| Handler(Some "exc", [ Expr(Call(Name "say", [ Name "exc" ])) ]))
    |> compileStmtFlip (withSymbols [ "say" ])
    |> hasCode
        [ PushTryContext 4
          LoadConst "error"
          ThrowException
          Jump 7
          StoreVar(1, 0)
          Pop
          LoadVar(0, 0)
          LoadVar(1, 0)
          FunctionCall 1
          Pop ]

[<Fact>]
let ``函数定义`` () =
    FunctionDef(
        "f",
        [],
        [ Let [ LetValue("x", Some <| Constant 1); LetValue("y", None) ]
          Return(
              Some
              <| Lambda(
                  [],
                  None,
                  [ Let [ LetValue("y", Some <| Constant 2) ]
                    Return(Some <| BinOp(Name "x", Add, Name "y")) ]
              )
          ) ]
    )
    |> compileStmtFlip initState
    |> hasCode
        [ MakeFunction(
              Some "f",
              0,
              2,
              [| LoadConst 1
                 StoreVar(0, 0)
                 Pop
                 MakeFunction(
                     None,
                     0,
                     1,
                     [| LoadConst 2
                        StoreVar(0, 0)
                        Pop
                        LoadVar(0, 1)
                        LoadVar(0, 0)
                        BinaryOperation ExpressionType.Add
                        ReturnValue |]
                 )
                 ReturnValue |]
          )
          StoreVar(0, 0) ]

let exports names (_, exports) =
    for name in names do
        exports |> should contain name

let hasCode' code (state, _) = hasCode code state
let initState' = initState, Set.empty
let withSymbols' symbols = withSymbols symbols, Set.empty

[<Fact>]
let ``导入和导出语句`` () =
    Import(ImportAll, Some "x", "path/to/lib")
    |> compileTopLevelFlip initState'
    |> hasCode' [ ImportModule "path/to/lib"; StoreVar(0, 0); Pop ]

    Import(ImportNames [ "foo" ], None, "path/to/lib")
    |> compileTopLevelFlip initState'
    |> hasCode' [ ImportModule "path/to/lib"; Copy; LoadAttr "foo"; StoreVar(0, 0); Pop; Pop ]

    Export [ ExportItem("a", Some <| Constant 1); ExportItem("b", None) ]
    |> compileTopLevelFlip (withSymbols' [ "b" ])
    |> both (exports [ "a"; "b" ]) (hasCode' [ LoadConst 1; StoreVar(1, 0); Pop ])

    ExportFunction("f", [], [])
    |> compileTopLevelFlip initState'
    |> both
        (exports [ "f" ])
        (hasCode'
            [ MakeFunction(Some "f", 0, 0, [| LoadConst null; ReturnValue |])
              StoreVar(0, 0)
              Pop ])
