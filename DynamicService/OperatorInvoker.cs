// Dynamitey不支持一元运算符中的UnaryPlus与OnesComplement, 得自己用C# dynamic实现.
// 顺便把二元运算也写了.

namespace DynamicService;

using Op = System.Linq.Expressions.ExpressionType;

public static class OperatorInvoker
{
    public static dynamic InvokeBinary(dynamic left, Op op, dynamic right) => op switch {
        Op.Add => left + right,
        Op.Divide => left / right,
        Op.Equal => left == right,
        Op.ExclusiveOr => left ^ right,
        Op.GreaterThan => left > right,
        Op.GreaterThanOrEqual => left >= right,
        Op.LeftShift => left << right,
        Op.LessThan => left < right,
        Op.LessThanOrEqual => left <= right,
        Op.Modulo => left % right,
        Op.Multiply => left * right,
        Op.NotEqual => left != right,
        Op.RightShift => left >> right,
        Op.Subtract => left - right,
        Op.Or => left | right,
        Op.And => left & right
    };

    public static dynamic InvokeUnary(Op op, dynamic operand) => op switch {
        Op.Not => !operand,
        Op.UnaryPlus => +operand,
        Op.Negate => -operand,
        Op.OnesComplement => ~operand
    };
}
