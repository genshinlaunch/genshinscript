dotnet publish -c Release -r win-x64 /p:PublishTrimmed=true /p:TrimMode=Link /p:IncludeNativeLibrariesForSelfExtract=true

Compress-Archive ./bin/Release/net7.0/win-x64/publish GenshinScript.zip

dotnet publish -c Release -r win-x64 /p:PublishSingleFile=true /p:PublishTrimmed=true /p:TrimMode=Link /p:IncludeNativeLibrariesForSelfExtract=true

Copy-Item ./bin/Release/net7.0/win-x64/publish/GenshinScript.exe .