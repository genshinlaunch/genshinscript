module Genshin.Repl

open System
open System.IO
open System.Text
open System.Text.RegularExpressions

open Genshin.Lang
open Genshin.Pervasive

open Ast
open Environment
open Exceptions

let private helpMsg =
    """

  源语言交互窗口指令:

    #help;; / #帮助；；                                       // 显示帮助
    #clear;; / #清除；；                                      // 清除屏幕
    #quit;; / #退出；；                                       // 退出


"""

let interact (banner: string) =
    let inputEnd = Regex(".*[;；]{2}$")
    let leadingSpaces = Regex("(\s*).*")
    Console.InputEncoding <- UnicodeEncoding() // 防止中文乱码.
    let ps1 = "> "
    let ps2 = "- "
    eprintfn "%s" banner

    let modulesManager =
        ModulesManager(Default.pervasive, System.Environment.CurrentDirectory)

    let env = Environment(modulesManager, Default.pervasive)

    let rec mainloop buffer symbols more =
        if more then ps2 else ps1
        |> printf "%s"

        match Console.ReadLine() with
        | null -> ()
        | line ->
            let lineTrimmed = line.TrimEnd()

            if inputEnd.IsMatch(lineTrimmed) then
                let line = lineTrimmed.Substring(0, lineTrimmed.Length - 2) + ";"
                let source = String.Join('\n', List.rev (line :: buffer))

                match source with
                | "#help;"
                | "#帮助;" ->
                    printfn "%s" helpMsg
                    mainloop [] symbols false
                | "#quit;"
                | "#退出;" -> ()
                | "#clear;"
                | "#清除;" ->
                    Console.Clear()
                    mainloop [] symbols false
                | _ ->
                    match env.TryExecute(source) with
                    | Ok result ->
                        let symbols = Util.updateMap symbols env.Symbols

                        for name in [ "it"; "那个" ] do
                            Module
                                [ if Map.containsKey name symbols then
                                      Assign(NameTarget name, Constant result) |> Expr |> Statement
                                  else
                                      Let [ LetValue(name, Some <| Constant result) ] |> Statement ]
                            |> env.TryExecuteAst
                            |> ignore

                        if not <| isNull result then
                            printfn "%A" result
                    | Error msg -> eprintfn "%s" msg

                    mainloop [] symbols false
            elif not <| String.IsNullOrWhiteSpace(lineTrimmed) then
                mainloop (lineTrimmed :: buffer) symbols true
            else
                mainloop buffer symbols more

    mainloop [] Map.empty false
