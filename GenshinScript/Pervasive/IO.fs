module Genshin.Pervasive.IO

open System

let exports =
    [ [ "say"; "说" ],
      Func<obj, obj>(fun x ->
          printfn "%O" x
          null)
      |> box
      [ "write"; "写" ],
      Func<obj, obj>(fun x ->
          printf "%O" x
          null)
      |> box
      [ "repr"; "显示" ], Func<obj, string>(sprintf "%A") |> box
      [ "display"; "打印" ],
      Func<obj, obj>(fun x ->
          printfn "%A" x
          null)
      |> box
      [ "read"; "读字" ], Func<char>(Console.Read >> char) |> box
      [ "readline"; "读行" ], Func<string>(Console.ReadLine) |> box ]
