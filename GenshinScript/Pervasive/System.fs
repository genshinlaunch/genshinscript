module Genshin.Pervasive.System

open System

let exports =
    [ [ "quit"; "exit"; "退出" ],
      Func<int, obj>(fun x ->
          Environment.Exit(x)
          null)
      |> box ]
