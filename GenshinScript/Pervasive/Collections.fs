module Genshin.Pervasive.Collections

open System
open System.Linq

let exports =
    [ [ "range"; "范围" ],
      Func<int, int, int seq>(fun start count -> Enumerable.Range(start, count))
      |> box ]
