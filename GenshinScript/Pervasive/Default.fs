module Genshin.Pervasive.Default

open Genshin.Lang.Environment

let ofList list =
    let symbols =
        seq {
            for (keys, _), i in Seq.zip list (Seq.initInfinite id) do
                for key in keys do
                    key, i
        }
        |> Map

    let values = list |> Seq.map snd |> Seq.toArray
    { Symbols = symbols; Values = values }

let pervasive =
    [ IO.exports
      About.exports
      Mscorlib.exports
      System.exports
      Types.exports
      Collections.exports
      Math.exports ]
    |> List.concat
    |> ofList
