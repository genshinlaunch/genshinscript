module Genshin.Pervasive.Math

open System
open System.Collections
open System.Dynamic

type GenshinRandom() =
    inherit DynamicObject()

    let rand = Random()

    override _.TryInvoke(binder, args, result) =
        match args with
        | [| :? int as upper |] ->
            result <- rand.Next(upper)
            true
        | [| :? int as lower; :? int as upper |] ->
            result <- rand.Next(lower, upper)
            true
        | [| :? IList as list |] ->
            result <- list[rand.Next(list.Count)]
            true
        | _ -> false

    override _.ToString() = "https://wiki.biligame.com/ys/抽卡模拟器"

let exports = [ [ "random"; "抽卡" ], GenshinRandom() |> box ]
