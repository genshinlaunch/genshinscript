module Genshin.Pervasive.Types

open System
open System.Collections
open System.Dynamic

open Dynamitey

open Genshin.Lang
open Evaluator
open Interop

type FunctionInvoker(func: Function) =
    inherit DynamicObject()

    override _.TryInvoke(binder, args, result) =
        result <- func.Invoke(args)
        true

let toDelegate (typeWrapper: TypeWrapper) (func: Function) =
    Dynamic.CoerceToDelegate(FunctionInvoker(func), typeWrapper.Wrapped)

let private seqToArrayList seq =
    let result = ArrayList()

    for i in seq do
        result.Add(i)

    result

let describeType (t: TypeWrapper) =
    let result = Object()
    let info = t.Info
    result["事件"] <- seqToArrayList <| Map.keys info.Events
    result["方法"] <- seqToArrayList info.Methods
    result["静态方法"] <- seqToArrayList info.StaticMethods
    result["属性"] <- seqToArrayList <| Map.keys info.Properties
    result["静态属性"] <- seqToArrayList <| Map.keys info.StaticProperties
    result["嵌套类型"] <- seqToArrayList <| Map.keys info.NestedTypes
    result

let exports =
    [ [ "delegate"; "委托" ], Func<_, _, _>(toDelegate) |> box
      [ "convert"; "转换" ],
      Func<obj, TypeWrapper, obj>(fun object ``type`` -> Dynamic.InvokeConvert(object, ``type``.Wrapped, true))
      |> box
      [ " issubclass"; "是子类" ],
      Func<TypeWrapper, TypeWrapper, bool>(fun t1 t2 -> t1.Wrapped.IsSubclassOf(t2.Wrapped))
      |> box
      [ "isinstance"; "是类型" ], Func<obj, TypeWrapper, bool>(fun x t -> t.Wrapped.IsInstanceOfType(x)) |> box ]
