module Genshin.Pervasive.Mscorlib

open System

open Genshin.Lang.Interop

let mscorlib = AssemblyWrapper(typeof<obj>.Assembly)

let exports =
    [ [ "mscorlib"; "微软核心库" ], mscorlib |> box
      [ "root"; "根" ], NamespaceWrapper("", Namespace.Root) |> box
      [ "system"; "系统" ], NamespaceWrapper("System", Namespace.Root.GetChildNamespace("System")) |> box
      [ "console"; "控制台" ], AssemblyWrapper(typeof<Console>.Assembly) |> box ] // System.Private.CoreLib.dll中没有System.Console.
