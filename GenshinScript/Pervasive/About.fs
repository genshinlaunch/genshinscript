module Genshin.Pervasive.About

open System

let exports =
    [ [ "about"; "关于" ],
      Func<obj>(fun () ->
          printfn "源语言由以下贡献者开发：**宁波市镇海区源深琦东软件工作室(原创开发), 联系邮箱zvmsbackend@outlook.com.**"
          null)
      |> box
      [ "version"; "版本" ], box "0.9.13" ]
