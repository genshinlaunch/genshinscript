namespace Genshin

open System.Collections.Generic

open FParsec

[<RequireQualifiedAccess>]
module Util =
    let flip f x y = f y x

    let zip xs ys =
        let rec loop acc xs ys =
            match xs, ys with
            | [], _
            | _, [] -> List.rev acc
            | x :: xs, y :: ys -> loop ((x, y) :: acc) xs ys

        loop [] xs ys

    let fold folder state list = List.fold (flip folder) state list

    let foldStateLast folder list state = fold folder state list

    let cons x y = x :: y

    let isFalse (obj: obj) = isNull obj || obj.Equals(false)

    let flattenDoublePair ((x, y), z) = x, y, z

    let applyTriple f x y z = f (x, y, z)

    let toResult pr =
        match pr with
        | Success(result, _, _) -> Result.Ok result
        | Failure(msg, _, _) -> Result.Error msg

    type ResultBuilder() =
        member _.Bind(x, f) =
            match x with
            | Result.Ok ok -> f ok
            | Result.Error error -> Result.Error error

        member _.Return(x) = Result.Ok x

        member _.ReturnFrom(x) = x

        member _.Zero() = Result.Ok()

    let result = ResultBuilder()

    let updateMap x y =
        Map.fold (fun s k v -> Map.add k v s) x y

    let (|ListLast|_|) list =
        let rec loop acc list =
            match list with
            | [ last ] -> List.rev acc, last
            | x :: xs -> loop (x :: acc) xs
            | [] -> invalidOp "列表为空" // 不会发生.

        match list with
        | [] -> None
        | _ -> Some <| loop [] list

    let mapt mapping seq' = seq { for i in seq' -> mapping i, i }

    let group2 pred mapping seq =
        let folder (yes, no) elem =
            if pred elem then
                Set.add (mapping elem) yes, no
            else
                yes, Set.add (mapping elem) no

        Seq.fold folder (Set.empty, Set.empty) seq

    let group2ToMap pred mapping key seq =
        let folder (yes, no) elem =
            let add = Map.add (key elem) (mapping elem)
            if pred elem then add yes, no else yes, add no

        Seq.fold folder (Map.empty, Map.empty) seq

    let fromCache (cache: Dictionary<'a, 'b>) (generate: 'a -> 'b) (key: 'a) =
        match cache.TryGetValue(key) with
        | true, result -> result
        | false, _ ->
            let result = generate key
            cache[key] <- result
            result
