module Genshin.Lang.Environment

open System
open System.Collections.Generic
open System.IO
open System.Reflection

open Genshin

open Compiler
open Evaluator
open Exceptions
open Interop

type Pervasive =
    { Symbols: Map<string, int>
      Values: obj[] }

type Environment(modulesManager, pervasive) =
    let locals = ref [||]
    let mutable symbols = Map.empty
    let mutable symcnt = 0
    let mutable exports = Set.empty

    member _.Symbols = symbols

    member this.GetObject() =
        let moduleObj = Object()

        for key in exports do
            moduleObj[key] <- locals.Value[Map.find key symbols]

        moduleObj

    member this.TryExecuteAst(ast) =
        let symtab = [ symbols; pervasive.Symbols ]

        Util.result {
            let! ``module`` = tryCompileModule symtab symcnt ast
            symbols <- ``module``.Symbols
            exports <- ``module``.Exports
            symcnt <- ``module``.SymbolsCount
            Array.Resize(locals, symcnt)

            let result =
                eval modulesManager ``module``.Code [ !locals; pervasive.Values.Clone() :?> obj[] ]

            return result
        }

    member this.TryExecute(source) =
        Util.result {
            let! ast = Parser.tryParse source
            return! this.TryExecuteAst(ast)
        }

    member this.Execute(source) =
        match this.TryExecute(source) with
        | Ok result -> result
        | Error msg -> raise <| SyntaxException msg

    member this.ExecuteFile(path, streamReader: StreamReader) =
        let source =
            try
                streamReader.ReadToEnd()
            with :? IOException ->
                raise <| ModuleNotFoundException $"无法打开{path}"

        let result = this.Execute(source)
        streamReader.Close()
        result

    member this.ExecuteFile(path) =
        let stream = new FileStream(path, FileMode.Open)
        let reader = new StreamReader(stream)
        this.ExecuteFile(path, reader)

type ModulesManager(pervasive, basePath) =
    static let imported = Dictionary()

    interface IModulesManager with
        member this.LoadModule(path) =
            let fullPath = Path.GetFullPath(Path.Combine(basePath, path))

            if not <| Path.Exists(fullPath) then
                raise <| ModuleNotFoundException $"{path}: 模块未找到"
            else
                match imported.TryGetValue(fullPath) with
                | true, ``module`` -> ``module``
                | false, _ ->
                    let moduleObj =
                        try
                            box <| AssemblyWrapper(Assembly.LoadFrom(fullPath))
                        with :? BadImageFormatException ->
                            let stream = new FileStream(path, FileMode.Open)
                            let reader = new StreamReader(stream)

                            let source =
                                try
                                    reader.ReadToEnd()
                                with :? IOException ->
                                    raise <| ModuleNotFoundException $"无法打开{path}"

                            reader.Close()
                            let newManager = ModulesManager(pervasive, fullPath)
                            let env = Environment(newManager, pervasive)
                            env.Execute(source) |> ignore
                            box <| env.GetObject()

                    imported[fullPath] <- moduleObj
                    moduleObj
