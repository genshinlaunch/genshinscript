module Genshin.Lang.Operators

open System.Linq.Expressions

open Ast

type private Op = ExpressionType

let getBinary op =
    match op with
    | Add -> Op.Add
    | Sub -> Op.Subtract
    | Mul -> Op.Multiply
    | Div -> Op.Divide
    | Mod -> Op.Modulo
    | LShift -> Op.LeftShift
    | RShift -> Op.RightShift
    | BitAnd -> Op.And
    | BitOr -> Op.Or
    | BitXor -> Op.ExclusiveOr
    | Eq -> Op.Equal
    | Ne -> Op.NotEqual
    | Lt -> Op.LessThan
    | Le -> Op.LessThanOrEqual
    | Gt -> Op.GreaterThan
    | Ge -> Op.GreaterThanOrEqual

let getUnary op =
    match op with
    | UAdd -> Op.UnaryPlus
    | Negate -> Op.Negate
    | Invert -> Op.OnesComplement
    | Not -> Op.Not
