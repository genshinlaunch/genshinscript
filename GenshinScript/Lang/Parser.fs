module Genshin.Lang.Parser

open System
open System.Globalization

open FParsec

open Genshin

open Ast
open Exceptions

module Internal =
    module Helpers =
        let (|->) f g x = f x || g x

        // 注释可以看作空白
        let spaces = regex @"\s*(//.*|/\*[\s\S]*\*/|\s*)*" >>% ()

        // 一些关键字不能与标识符相连, 但可以续以符号.
        // 比如, "return(a)"和"return a"是合法的, 而"returna"不是.
        let spaces' =
            optional (satisfy (Char.IsLetterOrDigit |-> (=) '_') >>. fail "空白") >>. spaces

        // 方便链式调用
        let inline sepBy x y = sepBy y x
        let inline sepBy1 x y = sepBy1 y x

        // 同时支持英语/半角和中文/全角.
        let token (result: 'a) eng chn label : Parser<'a, unit> =
            // 关键字需要空白, 符号不需要.
            let [ spacesEng; spacesChn ] =
                [ for t in [ eng; chn ] -> if String.forall Char.IsLetter t then spaces' else spaces ]

            (skipString eng .>> spacesEng) <|> (skipString chn .>> spacesChn) >>% result
            |> attempt
            <?> label

        // 让它直接返回unit
        let tokenu = token ()

        // 很多标记的标签和中文表示相同.
        let (<|!>) eng chn = tokenu eng chn chn

        // 考虑到语言习惯差异, 有些语法结构的中英文语序不同.
        // 比如, foreach (i in iterable)和遍历(iterable 中的 i).
        let (<|~>) eng chn =
            skipString eng >>. spaces' <?> eng, skipString chn >>. spaces' <?> chn

    open Helpers

    module Lexer =
        let lParen = tokenu "(" "（" "左括号"
        let rParen = tokenu ")" "）" "右括号"
        let lBracket = tokenu "[" "［" "左方括号"
        let rBracket = tokenu "]" "］" "右方括号"
        let lBrace = tokenu "{" "｛" "左花括号"
        let rBrace = tokenu "}" "｝" "右花括号"
        // 全角引号是我见过最愚蠢的符号.
        // 它其实是不被支持的.
        let lquote = tokenu "\"" "“" "左引号"
        let rquote = tokenu "\"" "”" "右引号"
        let lsquote = tokenu "'" "‘" "左单引号"
        let rsquote = tokenu "'" "’" "右单引号"
        let comma = tokenu "," "，" "逗号"
        let semicolon = tokenu ";" "；" "分号"
        let colon = tokenu ":" "：" "冒号"
        let dot = tokenu "." "－＞" "点号/瘦箭头"
        let arrow = tokenu "=>" "＝＞" "胖箭头"
        let questionMark = tokenu "?" "？" "问号"
        let assign = tokenu "=" "设为" "设为"
        let eq = token Eq "==" "等于" "双等号"
        let ne = token Ne "<>" "不等于" "不等号"
        let gt = token Gt ">" "大于" "大于"
        let ge = token Ge ">=" "大于等于" "大于等于"
        let lt = token Lt "<" "小于" "小于"
        let le = token Le "<=" "小于等于" "小于等于"
        let add = token Add "+" "加" "加"
        let sub = token Sub "-" "减" "减"
        let mul = token Mul "*" "乘" "乘"
        let div = token Div "/" "除" "除"
        let mod' = token Mod "%" "模" "百分号"
        let bitand = token BitAnd "&" "位与" "与"
        let bitor = token BitOr "|" "位或" "或"
        let bitxor = token BitXor "^" "异或" "异或"
        let lshift = token LShift "<<" "左移" "右移"
        let rshift = token RShift ">>" "右移" "左移"
        let and' = tokenu "&&" "且" "且"
        let or' = tokenu "||" "或" "或"
        let addeq = token Add "+=" "自加" "加等"
        let subeq = token Sub "-=" "自减" "减等"
        let muleq = token Mul "*=" "自乘" "乘等"
        let diveq = token Div "/=" "自除" "除等"
        let modeq = token Mod "%=" "自模" "模等"
        let andeq = token BitAnd "&=" "自与" "与等"
        let oreq = token BitOr "|=" "自或" "或等"
        let xoreq = token BitXor "^=" "自异或" "异或等"
        let lshifteq = token LShift "<<=" "自左移" "左移等"
        let rshifteq = token RShift ">>=" "自右移" "右移等"
        let uadd = token UAdd "+" "正" "正"
        let negate = token Negate "-" "负" "负"
        let invert = token Invert "~" "取反" "波浪号/取反"
        let not' = token Not "!" "非" "非"
        let fun' = "fun" <|!> "函数"
        let if' = "if" <|!> "如果"
        let else' = "else" <|!> "否则"
        let while' = "while" <|!> "当"
        let for' = "for" <|!> "遍历"
        let foreachEng, foreachChn = "foreach" <|~> "对于"
        let inEng, inChn = "in" <|~> "中的"
        let try' = "try" <|!> "尝试"
        let catch = "catch" <|!> "排查"
        let this = token This "this" "自身" "自身"
        let break' = token Break "break" "跳出" "跳出"
        let continue' = token Continue "continue" "继续" "继续"
        let null' = token (Constant null) "null" "空" "空"
        let true' = token (Constant true) "true" "真" "真"
        let false' = token (Constant false) "false" "假" "假"
        let return' = "return" <|!> "返回"
        let throw = "throw" <|!> "抛出"
        let let' = "let" <|!> "给定"
        let importEng, importChn = "import" <|~> "导入"
        let asEng, asChn = "as" <|~> "为"
        let fromEng, fromChn = "from" <|~> "从"
        let export = "export" <|!> "导出"

        let inline sepByCommas parser = parser |> sepBy comma

        let inline sepByCommas1 parser = parser |> sepBy1 comma

        let inline betweenParens parser = parser |> between lParen rParen

        let inline betweenBrackets parser = parser |> between lBracket rBracket

        let inline betweenBraces parser = parser |> between lBrace rBrace

    open Lexer

    module ForwardReference =
        let oExpr', oExprRef = createParserForwardedToRef<Expr, unit> ()
        let oStatement', oStatementRef = createParserForwardedToRef<Statement, unit> ()
        let oExpr = oExpr' <?> "表达式"
        let oStatement = oStatement' <?> "语句"

    open ForwardReference

    module Elements =
        let ident = IdentifierOptions() |> identifier .>> spaces' <?> "标识符"

        let parameters = ident |> sepByCommas |> betweenParens

        let lambdaParameters =
            (parameters .>>. opt ident)
            <|> pipe2 ident (opt ident) (fun id name -> [ id ], name)
            .>> arrow

        let block = oStatement |> many |> betweenBraces

        let statements = oStatement |>> List.singleton <|> block

        let ochar, quotedString =
            let unescapedChar = noneOf "\\\"“"

            let escapedChar =
                [ ("\\\"", '"')
                  ("\\”", '”')
                  ("\\\\", '\\')
                  ("\\/", '/')
                  ("\\b", '\b')
                  ("\\f", '\f')
                  ("\\n", '\n')
                  ("\\r", '\r')
                  ("\\t", '\t') ]
                |> List.map (fun (toMatch, result) -> stringReturn toMatch result)
                |> choice

            let unicodeChar =
                let convertToChar (s: string) =
                    Int32.Parse(s.Substring(2), NumberStyles.HexNumber) |> char

                regex @"\\u\d{4}" |>> convertToChar

            let ochar = choice [ unescapedChar; escapedChar; unicodeChar ]

            ochar, manyChars ochar |> between lquote rquote

    open Elements

    module Factor =
        let oNull = null'

        let oBool = true' <|> false' <?> "布尔"

        let oChar = ochar |> between lsquote rsquote |>> (box >> Constant) <?> "字符"

        let oString = quotedString |>> (box >> Constant) <?> "字符串"

        // 可能超出数字范围.
        let tryConvert convert msg (s: string) =
            match convert s with
            | true, res -> preturn (Constant res)
            | false, _ -> fail msg

        let oInt =
            let converts =
                // F#值不能为泛型, 故无法使用partial application
                let msg = "无效整数字面量"

                [ "", tryConvert Int32.TryParse msg
                  "u", tryConvert UInt32.TryParse msg
                  "s", tryConvert Int16.TryParse msg
                  "us", tryConvert UInt16.TryParse msg
                  "y", tryConvert SByte.TryParse msg
                  "uy", tryConvert Byte.TryParse msg
                  "I", tryConvert bigint.TryParse msg ]
                |> Map

            let f (intPart, suffix) = Map.find suffix converts <| intPart

            regex @"([1-9]\d*|0)" .>>. regex "(u|s|us|y|uy|I)?" >>= f .>> spaces' <?> "整数"

        // 虽然Decimal不是浮点数, 但放一起比较方便.
        let oFloat =
            let pattern = @"([1-9]\d*|0)\.(\d*[1-9]|0)([eE][+-]?\d+)?"

            let oFloat =
                let f (floatPart, maybeF) =
                    let msg = "无效浮点字面量"

                    match maybeF with
                    | Some 'f' -> tryConvert Single.TryParse msg floatPart
                    | Some 'M' -> tryConvert Decimal.TryParse msg floatPart
                    | None -> tryConvert Double.TryParse msg floatPart

                regex pattern .>>. opt (anyOf "fM") >>= f .>> spaces' <?> "浮点数"

            oFloat

        let oArray = oExpr .>> spaces |> sepByCommas |> betweenBrackets |>> Array <?> "数组"

        let oObject =
            let idxKey = oExpr |> betweenBrackets

            let entry =
                let attrEntry = ident .>> colon .>>. oExpr |>> AttributeEntry
                let idxEntry = idxKey .>> colon .>>. oExpr |>> IndexEntry

                let funcEntry =
                    ident .>>. parameters .>>. block |> attempt
                    |>> (Util.flattenDoublePair >> FunctionEntry)

                choice [ funcEntry; attrEntry; idxEntry ]

            entry |> sepByCommas |> betweenBraces |>> Object <?> "对象"

        let atom, atomRef = createParserForwardedToRef ()

        let oName = ident |>> Name
        let oConstant = choice [ oNull; oBool; oFloat; oInt; oString; oChar ]
        let oThis = this

        let parenthesized = oExpr |> betweenParens

        atomRef.Value <-
            let args = oExpr |> sepByCommas |> betweenParens |> opt
            let start = choice [ oConstant; oThis; oName; oArray; oObject; parenthesized ]

            // 有什么更好的办法吗?
            let rec loop acc =
                parse {
                    match! args with
                    | Some args -> return! loop <| Call(acc, args)
                    | None ->
                        match! opt (dot >>. ident) with
                        | Some attr -> return! loop <| Attribute(acc, attr)
                        | None ->
                            match! opt (oExpr |> sepByCommas1 |> betweenBrackets) with
                            | Some idx -> return! loop <| Subscript(acc, idx)
                            | None -> return acc
                }

            start >>= loop

        let factor =
            let ops = choice [ uadd; negate; invert; not' ]
            ops .>>. atom |>> UnaryOp <|> atom

    open Factor

    module BinOp =
        let makeBinOp operand ops =
            let maybeOpAndRight = choice ops .>>. operand |> attempt |> opt

            let rec loop acc =
                parse {
                    match! maybeOpAndRight with
                    | Some(op, right) -> return! loop <| BinOp(acc, op, right)
                    | None -> return acc
                }

            operand >>= loop

        let term = makeBinOp factor [ mul; div; mod' ]
        let arithExpr = makeBinOp term [ add; sub ]
        let shiftExpr = makeBinOp arithExpr [ lshift; rshift ]
        let andExpr = makeBinOp shiftExpr [ bitand ]
        let xorExpr = makeBinOp andExpr [ bitor; bitxor ]
        let comparison = makeBinOp xorExpr [ eq; ne; le; lt; ge; gt ]

        let makeTest operand seperator op =
            let f values =
                match values with
                | [ value ] -> value
                | _ -> BoolOp(op, values)

            operand |> sepBy1 seperator |>> f

        let andTest = makeTest comparison and' And
        let orTest = makeTest andTest or' Or

        let exprToTarget expr =
            match expr with
            | Name name -> preturn <| NameTarget name
            | Attribute(value, attr) -> preturn <| AttributeTarget(value, attr)
            | Subscript(value, indices) -> preturn <| SubscriptTarget(value, indices)
            | _ -> fail "不可赋值"

        let expr =
            let augOps =
                choice [ addeq; subeq; muleq; diveq; modeq; andeq; oreq; xoreq; lshifteq; rshifteq ]

            parse {
                let! first = orTest

                match! opt (exprToTarget first) with
                | Some target ->
                    match! opt augOps with
                    | Some op ->
                        let! value = oExpr
                        return AugAssign(target, op, value)
                    | None ->
                        match! opt assign with
                        | Some _ ->
                            let! value = oExpr
                            return Assign(target, value)
                        | None -> return first
                | None -> return first
            }

    open BinOp

    module Expr =

        let oLambda =
            parse {
                let! (parameters, name) = lambdaParameters

                match! opt block with
                | Some body -> return Lambda(parameters, name, body)
                | None ->
                    let! body = oExpr
                    return SimpleLambda(parameters, name, body)
            }

        oExprRef
        := let ifExprTail = spaces >>. questionMark >>. oExpr .>> colon .>>. oExpr in

           let start = choice [ attempt expr; oLambda ] in

           let rec loop acc =
               parse {
                   match! opt ifExprTail with
                   | Some(body, orelse) -> return! loop <| IfExpr(acc, body, orelse)
                   | None -> return acc
               } in

           start >>= loop

    module Statement =
        let expr = oExpr .>> semicolon |> attempt |>> Expr
        let oContinue = continue' .>> semicolon
        let oBreak = break' .>> semicolon
        let oReturn = return' >>. opt oExpr .>> semicolon |>> Return
        let oThrow = throw >>. oExpr .>> semicolon |>> Throw
        let oPass = semicolon >>% Pass

        let letBase =
            let oLetItem =
                let LetProperties =
                    ident |> sepByCommas1 |> betweenBraces .>> assign .>>. oExpr |>> LetProperties

                let LetValue = ident .>>. opt (assign >>. oExpr) |>> LetValue

                LetProperties <|> LetValue

            let' >>. (oLetItem |> sepByCommas1)

        let oLet = letBase .>> semicolon |>> Let

        let oIf =
            if' >>. parenthesized .>>. statements .>>. opt (else' >>. statements)
            |>> (Util.flattenDoublePair >> If)

        let oWhile = while' >>. parenthesized .>>. statements |>> While

        let oForEach =
            let eng =
                foreachEng >>. lParen >>. ident .>> inEng .>>. oExpr .>> rParen .>>. statements
                |>> (Util.flattenDoublePair >> ForEach)

            let chn =
                let f ((iter, target), body) = ForEach(target, iter, body)

                foreachChn >>. lParen >>. oExpr .>> inChn .>>. ident .>> rParen .>>. statements
                |>> f

            eng <|> chn

        let oFor =
            let oForInit =
                let exprInit = oExpr |>> ExprInit
                let letInit = letBase |>> LetInit
                letInit <|> exprInit

            let f (((init, test), step), body) = For(init, test, step, body)

            for' >>. lParen >>. opt oForInit .>> semicolon .>>. opt oExpr .>> semicolon
            .>>. opt oExpr
            .>> rParen
            .>>. statements
            |>> f

        let oTry =
            let f (body, maybeHandler) =
                Try(body, Option.map Handler maybeHandler)

            try' >>. statements
            .>>. opt (catch >>. opt (ident |> betweenParens |> attempt) .>>. statements)
            |>> f

        let oFunctionDef =
            fun' >>. ident .>>. parameters .>>. block
            |>> (Util.flattenDoublePair >> FunctionDef)

        oStatementRef
        := choice
            [ oLet
              oIf
              oWhile
              oForEach
              oFor
              oTry
              oThrow
              oFunctionDef
              oReturn
              oContinue
              oBreak
              oPass
              expr ]

    module TopLevel =
        let statement = oStatement |>> Statement

        let oImport =
            let oImportTarget =
                let importAll = charReturn '*' ImportAll .>> spaces
                let importNames = ident |> sepByCommas1 |> betweenBraces |>> ImportNames
                importAll <|> importNames

            let eng =
                importEng >>. oImportTarget .>>. opt (asEng >>. ident |> attempt) .>> fromEng
                .>>. quotedString
                .>> semicolon
                |>> (Util.flattenDoublePair >> Import)

            let chn =
                let f ((path, target), asname) = Import(target, asname, path)

                fromChn >>. quotedString .>> importChn
                .>>. oImportTarget
                .>>. opt (asChn >>. ident |> attempt)
                .>> semicolon
                |>> f

            eng <|> chn <?> "导入"

        let oExport =
            let oExportItem = ident .>>. opt (assign >>. oExpr) |>> ExportItem

            export >>. (oExportItem |> sepByCommas1) .>> semicolon |>> Export <?> "导出"

        let oExportFunction =
            export >>. ident .>>. parameters .>>. block
            |>> (Util.flattenDoublePair >> ExportFunction)
            <?> "导出函数"

        let oTopLevel = choice [ oImport; attempt oExport; oExportFunction; statement ]

    open TopLevel

    let oModule = spaces >>. many oTopLevel .>> eof |>> Module

let parse source =
    match run Internal.oModule source with
    | Success(result, _, _) -> result
    | Failure(msg, _, _) -> raise <| SyntaxException msg

let tryParse source =
    run Internal.oModule source |> Util.toResult
