module Genshin.Lang.Interop

open System
open System.Collections.Generic
open System.Dynamic
open System.Reflection
open System.Text.RegularExpressions

open Dynamitey

open Genshin

let private invokeMethod (``type``: Type) instance name args =
    match ``type``.GetMethod(name, Array.map (fun arg -> arg.GetType()) args) with
    | null -> None
    | mth -> Some <| mth.Invoke(instance, args)

type PropertyInfo =
    { GetMethod: MethodInfo option
      SetMethod: MethodInfo option }

type TypeInfo =
    { Methods: Set<string> // 方法允许重载, 因此只保存名字.
      StaticMethods: Set<string>
      Properties: Map<string, PropertyInfo>
      StaticProperties: Map<string, PropertyInfo>
      Events: Map<string, EventInfo>
      NestedTypes: Map<string, Type> }

type Reflector private () =
    static let instance = Reflector()

    let cache = Dictionary<Type, TypeInfo>()

    let getTypeInfo (``type``: Type) =
        let staticMethods, methods =
            ``type``.GetMethods()
            |> Util.group2 (fun mth -> mth.IsStatic) (fun mth -> mth.Name)

        let staticProperties, properties =
            let pred (prop: Reflection.PropertyInfo) =
                match prop.GetMethod with
                | null -> prop.SetMethod.IsStatic
                | _ -> prop.GetMethod.IsStatic

            let mapping (prop: Reflection.PropertyInfo) =
                { GetMethod = Option.ofObj prop.GetMethod
                  SetMethod = Option.ofObj prop.SetMethod }

            ``type``.GetProperties()
            |> Util.group2ToMap pred mapping (fun prop -> prop.Name)

        let events = ``type``.GetEvents() |> Util.mapt (fun e -> e.Name) |> Map.ofSeq

        let nestedTypes =
            ``type``.GetNestedTypes() |> Util.mapt (fun t -> t.Name) |> Map.ofSeq

        { Methods = methods
          StaticMethods = staticMethods
          Properties = properties
          StaticProperties = staticProperties
          Events = events
          NestedTypes = nestedTypes }

    static member Instance = instance

    member _.Reflect(``type``) =
        Util.fromCache cache getTypeInfo ``type``

type EventWrapper(instance, ``event``: EventInfo) =
    member _.Add(handler) =
        ``event``.AddEventHandler(instance, handler)

    member _.Remove(handler) =
        ``event``.RemoveEventHandler(instance, handler)

    override _.ToString() = $"<事件 {``event``.Name}>"

type StaticMethodWrapper(``type``, name) =
    inherit DynamicObject()

    member _.Type = ``type``
    member _.Name = name

    override _.TryInvoke(binder, args, result) =
        match invokeMethod ``type`` null name args with
        | None -> false
        | Some x ->
            result <- x
            true

    override _.ToString() = $"<静态方法 {``type``.Name}.{name}>"

type MethodWrapper(instance, name) =
    inherit DynamicObject()

    member _.Instance = instance
    member _.Name = name

    override _.TryInvoke(binder, args, result) =
        match invokeMethod (instance.GetType()) instance name args with
        | None -> false
        | Some x ->
            result <- x
            true

    override _.ToString() =
        $"<方法 {instance.GetType().Name}.{name}>"

type MaybeGenericType =
    | GenericType of name: string * argsCount: int
    | NonGenericType

    static member private GenericTypeName = Regex(@"^(.+?)(`\d+)?$")

    static member Parse(``type``: Type) =
        if ``type``.IsGenericType then
            let genericTypeNameMatch = MaybeGenericType.GenericTypeName.Match(``type``.Name)
            let name = genericTypeNameMatch.Groups[1].Value
            let argsCount = (``type`` :?> Reflection.TypeInfo).GenericTypeParameters.Length
            GenericType(name, argsCount)
        else
            NonGenericType


type TypeWrapper private () =
    inherit DynamicObject()

    static let raiseNotConcrete () = invalidOp "不是具体类型."

    let mutable typeAndTypeInfo: (Type * TypeInfo) option = None
    let mutable genericTypes = Dictionary<int, Type>()

    member _.Wrapped =
        match typeAndTypeInfo with
        | Some(``type``, _) -> ``type``
        | None -> raiseNotConcrete ()

    member _.Info =
        match typeAndTypeInfo with
        | Some(_, typeInfo) -> typeInfo
        | None -> raiseNotConcrete ()

    new(``type``) as this =
        TypeWrapper()
        then this.AddNonGenericDefinition(``type``)

    static member PureGeneric(argsCount, ``type``) =
        let result = TypeWrapper()
        result.AddGenericDefinition(argsCount, ``type``)
        result

    member _.AddNonGenericDefinition(def) =
        typeAndTypeInfo <- Some(def, Reflector.Instance.Reflect(def))

    member _.AddGenericDefinition(argsCount, def) = genericTypes[argsCount] <- def

    override _.TryInvoke(binder, args, result) =
        match typeAndTypeInfo with
        | Some(``type``, _) ->
            result <- Dynamic.InvokeConstructor(``type``, args = args)
            true
        | None -> false

    override _.TryGetMember(binder, result) =
        match typeAndTypeInfo with
        | Some(``type``, typeInfo) ->
            if Set.contains binder.Name typeInfo.StaticMethods then
                result <- StaticMethodWrapper(``type``, binder.Name)
                true
            else
                match Map.tryFind binder.Name typeInfo.NestedTypes with
                | Some nestedType ->
                    result <- TypeWrapper(nestedType)
                    true
                | None ->
                    match Map.tryFind binder.Name typeInfo.StaticProperties with
                    | Some { GetMethod = Some getter } ->
                        result <- getter.Invoke(null, null)
                        true
                    | _ -> false
        | None -> raiseNotConcrete ()

    override _.TrySetMember(binder, value) =
        match typeAndTypeInfo with
        | Some(_, typeInfo) ->
            match Map.tryFind binder.Name typeInfo.StaticProperties with
            | Some { SetMethod = Some setter } ->
                setter.Invoke(null, [| value |]) |> ignore
                true
            | _ -> false
        | None -> raiseNotConcrete ()

    override _.TryGetIndex(binder, indexes, result) =
        let typeArgs =
            [| for index in indexes do
                   match index with
                   | :? TypeWrapper as t -> t.Wrapped
                   | _ -> invalidOp $"{index}不是类型" |]

        match genericTypes.TryGetValue(indexes.Length) with
        | true, genericType ->
            result <- TypeWrapper(genericType.MakeGenericType(typeArgs))
            true
        | false, _ -> false

    override _.ToString() =
        match typeAndTypeInfo with
        | Some(``type``, _) -> $"<类型 {``type``.Name}>"
        | None -> "<泛型类型>"

type NamespaceChild =
    | ChildType of TypeWrapper
    | ChildNamespace of Namespace
    | Nothing

and Namespace() =
    member val private ChildTypes = Dictionary<string, TypeWrapper>() with get
    member val private ChildNamespaces = Dictionary<string, Namespace>() with get

    member this.Resolve(segments) =
        let folder (ns: Namespace) segment =
            match ns.ChildNamespaces.TryGetValue(segment) with
            | true, next -> next
            | false, _ ->
                let newNs = Namespace()
                ns.ChildNamespaces[segment] <- newNs
                newNs

        Seq.fold folder this segments

    member this.AddType(``type``: Type) =
        let ns = this.Resolve(``type``.Namespace.Split('.'))

        match MaybeGenericType.Parse(``type``) with
        | GenericType(name, argsCount) ->
            match ns.ChildTypes.TryGetValue(name) with
            | true, theType -> theType.AddGenericDefinition(argsCount, ``type``)
            | false, _ -> ns.ChildTypes[name] <- TypeWrapper.PureGeneric(argsCount, ``type``)
        | NonGenericType ->
            match ns.ChildTypes.TryGetValue(``type``.Name) with
            | true, theType -> theType.AddNonGenericDefinition(``type``)
            | false, _ -> ns.ChildTypes[``type``.Name] <- TypeWrapper(``type``)

    member this.GetChild(name) =
        match this.ChildTypes.TryGetValue(name) with
        | true, childType -> ChildType childType
        | false, _ ->
            match this.ChildNamespaces.TryGetValue(name) with
            | true, childNamespace -> ChildNamespace childNamespace
            | false, _ -> Nothing

    member this.GetChildNamespace(name) = this.ChildNamespaces[name]

    static member val Root = Namespace() with get

type NamespaceWrapper(name: string, ns: Namespace) =
    inherit DynamicObject()

    member _.Name = name

    override _.TryGetMember(binder, result) =
        match ns.GetChild(binder.Name) with
        | ChildType childType ->
            result <- childType
            true
        | ChildNamespace childNamespace ->
            let newName = if name = "" then binder.Name else $"{name}.{binder.Name}"

            result <- NamespaceWrapper(newName, childNamespace)
            true
        | Nothing -> false

    override _.ToString() = $"<命名空间 {``name``}>"

type AssemblyWrapper(asm: Assembly) =
    inherit DynamicObject()

    let childTypes, childNamespaces =
        let folder (types, nss) (theType: Type) =
            if String.IsNullOrEmpty(theType.Namespace) then
                Map.add theType.Name theType types, nss
            else
                let segments = theType.Namespace.Split('.')
                Namespace.Root.AddType(theType)
                types, Map.add segments[0] (Namespace.Root.GetChildNamespace(segments[0])) nss

        Seq.fold folder (Map.empty, Map.empty) <| asm.GetTypes()

    member _.Wrapped = asm

    override _.TryGetMember(binder, result) =
        match Map.tryFind binder.Name childTypes with
        | Some childType ->
            result <- TypeWrapper(childType)
            true
        | None ->
            match Map.tryFind binder.Name childNamespaces with
            | Some childNamespace ->
                result <- NamespaceWrapper(binder.Name, childNamespace)
                true
            | None -> false

    override _.ToString() = $"<程序集 {asm}>"

let getInstanceMember instance name : obj option =
    let typeInfo = Reflector.Instance.Reflect(instance.GetType())

    if Set.contains name typeInfo.Methods then
        Some <| MethodWrapper(instance, name)
    else
        match Map.tryFind name typeInfo.Events with
        | Some e -> Some <| EventWrapper(instance, e)
        | None -> None
