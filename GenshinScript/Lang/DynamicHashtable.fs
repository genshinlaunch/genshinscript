// Object类和Function类循环引用, 这在F#中本是不允许的.
// 因此在这个文件里实现抽象基类DynamicHashtable, 只把引用了Function的TryItem方法留到Evaluator.fs实现.

namespace Genshin.Lang

open System
open System.Collections
open System.Dynamic
open System.Runtime.Serialization

[<AbstractClass; AllowNullLiteral>]
type DynamicHashtable internal (table: Hashtable) =
    inherit DynamicObject()

    member internal _.Table = table

    // 抽象成员.
    abstract TryItem: obj -> obj option
    abstract Clone: unit -> obj

    override this.ToString() =
        let entries =
            seq {
                for entry in this ->
                    let entry = entry :?> DictionaryEntry
                    sprintf "%A: %A" entry.Key entry.Value
            }

        "{" + String.Join(", ", entries) + "}"

    // 重定向Hashtable的成员.
    member _.Count = table.Count
    member _.IsFixedSize = table.IsFixedSize
    member _.IsReadOnly = table.IsReadOnly
    member _.IsSynchronized = table.IsSynchronized

    member this.Item
        with get key =
            match this.TryItem(key) with
            | Some value -> value
            | None -> null
        and set key value = table[key] <- value

    member _.Keys = table.Keys
    member _.SyncRoot = table.SyncRoot
    member _.Values = table.Values
    member _.Add(key, value) = table.Add(key, value)
    member _.Clear() = table.Clear()
    member _.Contains(key) = table.Contains(key)
    member _.ContainsKey(key) = table.ContainsKey(key)
    member _.ContainsValue(value) = table.ContainsValue(value)
    member _.CopyTo(array, arrayIndex) = table.CopyTo(array, arrayIndex)
    member _.GetEnumerator() = table.GetEnumerator()
    member _.GetObjectData(info, context) = table.GetObjectData(info, context)
    member _.OnDeserialization(sender) = table.OnDeserialization(sender)
    member _.Remove(key) = table.Remove(key)

    static member private MemberNames =
        Set
            [ "Count"
              "IsFixedSize"
              "IsReadOnly"
              "IsSynchronized"
              "Keys"
              "SyncRoot"
              "Values"
              "Add"
              "Clear"
              "Contains"
              "ContainsKey"
              "ContainsValue"
              "CopyTo"
              "GetEnumerator"
              "GetObjectData"
              "OnDeserialization"
              "Remove" ]

    // implement Hashtable's interfaces.
    interface IEnumerable with
        member _.GetEnumerator() = table.GetEnumerator()

    interface ICloneable with
        member this.Clone() = this.Clone()

    interface ICollection with
        member _.Count = table.Count
        member _.IsSynchronized = table.IsSynchronized
        member _.SyncRoot = table.SyncRoot
        member _.CopyTo(array, arrayIndex) = table.CopyTo(array, arrayIndex)

    interface IDictionary with
        member _.IsFixedSize = table.IsFixedSize
        member _.IsReadOnly = table.IsReadOnly

        member this.Item
            with get idx =
                match this.TryItem(idx) with
                | Some value -> value
                | None -> null
            and set idx value = table[idx] <- value

        member _.Keys = table.Keys
        member _.Values = table.Values
        member _.Clear() = table.Clear()
        member _.Add(key, value) = table.Add(key, value)
        member _.Contains(key) = table.Contains(key)
        member _.GetEnumerator() = table.GetEnumerator()
        member _.Remove(key) = table.Remove(key)

    interface IDeserializationCallback with
        member _.OnDeserialization(sender) = table.OnDeserialization(sender)

    interface ISerializable with
        member _.GetObjectData(info, context) = table.GetObjectData(info, context)

    // 重写DynamicObject的成员.
    override _.GetDynamicMemberNames() =
        let filterStringKeys (keys: ICollection) =
            seq {
                for key in keys do
                    match key with
                    | :? string as s -> s
                    | _ -> ()
            }

        Set.union DynamicHashtable.MemberNames (table.Keys |> filterStringKeys |> Set)

    override this.TryGetIndex(binder, indexes, result) =
        match indexes with
        | [| idx |] ->
            match this.TryItem(idx) with
            | Some value ->
                result <- value
                true
            | None -> false
        | _ -> false

    override this.TryGetMember(binder, result) =
        match this.TryItem(binder.Name) with
        | Some value -> result <- value
        | None -> result <- null

        true

    override _.TrySetIndex(binder, indexes, value) =
        match indexes with
        | [| idx |] ->
            table[idx] <- value
            true
        | _ -> false

    override _.TrySetMember(binder, value) =
        table[binder.Name] <- value
        true
