module Genshin.Lang.Ast

type Operator =
    | Add
    | Sub
    | Mul
    | Div
    | Mod
    | BitAnd
    | BitOr
    | BitXor
    | LShift
    | RShift
    | Eq
    | Ne
    | Lt
    | Le
    | Gt
    | Ge

type BoolOperator =
    | And
    | Or

type UnaryOperator =
    | UAdd
    | Negate
    | Invert
    | Not

type Expr =
    | Call of func: Expr * args: Expr list
    | Name of id: string
    | Constant of value: obj
    | BinOp of left: Expr * op: Operator * right: Expr
    | UnaryOp of op: UnaryOperator * operand: Expr
    | BoolOp of op: BoolOperator * values: Expr list
    | Attribute of value: Expr * attr: string
    | Subscript of value: Expr * indexes: Expr list
    | Assign of target: Target * value: Expr
    | AugAssign of target: Target * op: Operator * value: Expr
    | Lambda of parameters: string list * name: string option * body: Statement list
    | SimpleLambda of parameters: string list * name: string option * body: Expr
    | IfExpr of test: Expr * body: Expr * orelse: Expr
    | Array of elts: Expr list
    | Object of entries: ObjectEntry list
    | This

and ObjectEntry =
    | FunctionEntry of name: string * parameters: string list * body: Statement list
    | AttributeEntry of attr: string * value: Expr
    | IndexEntry of index: Expr * value: Expr

and Target =
    | NameTarget of name: string
    | AttributeTarget of value: Expr * attr: string
    | SubscriptTarget of value: Expr * indexes: Expr list

and Statement =
    | Expr of value: Expr
    | Let of items: LetItem list
    | If of test: Expr * body: Statement list * orelse: Statement list option
    | While of test: Expr * body: Statement list
    | ForEach of target: string * iter: Expr * body: Statement list
    | For of init: ForInit option * test: Expr option * step: Expr option * body: Statement list
    | Try of body: Statement list * handler: Handler option
    | Throw of exc: Expr
    | FunctionDef of name: string * parameters: string list * body: Statement list
    | Return of value: Expr option
    | Continue
    | Break
    | Pass

and ForInit =
    | ExprInit of expr: Expr
    | LetInit of items: LetItem list

and Handler = Handler of name: string option * body: Statement list

and LetItem =
    | LetProperties of props: string list * value: Expr
    | LetValue of name: string * value: Expr option

type ImportTarget =
    | ImportAll
    | ImportNames of string list

type ExportItem = ExportItem of name: string * value: Expr option

type TopLevel =
    | Import of target: ImportTarget * asname: string option * path: string
    | Export of items: ExportItem list
    | ExportFunction of name: string * parameters: string list * body: Statement list
    | Statement of statement: Statement

type Module = Module of body: TopLevel list
