module Genshin.Lang.Exceptions

exception SyntaxException of string

exception ModuleNotFoundException of string

exception InvalidCommandlineOptionsException of string

exception GenshinException of obj * string list
