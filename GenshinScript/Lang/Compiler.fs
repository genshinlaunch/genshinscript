module Genshin.Lang.Compiler

open System.Linq.Expressions

open Genshin

open Ast
open Exceptions

type Instruction =
    | Nop
    | LoadConst of value: obj
    | LoadVar of slot: int * frame: int
    | LoadSubscr of indexesCount: int
    | LoadAttr of attr: string
    | StoreVar of slot: int * frame: int
    | StoreSubscr of indexesCount: int
    | StoreAttr of attr: string
    | Pop
    | Copy
    | Jump of delta: int
    | PopJumpIfFalse of delta: int
    | JumpIfFalseOrPop of delta: int
    | JumpIfTrueOrPop of delta: int
    | BinaryOperation of op: ExpressionType
    | UnaryOperation of op: ExpressionType
    | FunctionCall of argsCount: int
    | MakeArray of eltsCount: int
    | MakeObject of entriesCount: int
    | LoadThis
    | MakeFunction of name: string option * paramsCount: int * localsCount: int * code: Instruction[]
    | ReturnValue
    | PushTryContext of handler: int
    | GetIter
    | ForIter of delta: int
    | ThrowException
    | ImportModule of path: string
    // 下面的两条不是真的指令. 它们只是循环体编译完全前的占位符.
    | IncompleteBreak
    | InCompleteContinue

type State =
    { UnitName: string
      IsInLoop: bool
      SymbolTable: Map<string, int> list
      SymbolsCount: int
      Code: Instruction list
      CodeLength: int }

type Module =
    { Code: Instruction[]
      Symbols: Map<string, int>
      SymbolsCount: int
      Exports: Set<string> }

let initState =
    { UnitName = "<模块>"
      IsInLoop = false
      SymbolTable = [ Map.empty ]
      SymbolsCount = 0
      Code = []
      CodeLength = 0 }

let private addCode code state =
    { state with
        Code = Util.fold Util.cons state.Code code
        CodeLength = state.CodeLength + List.length code }

let private fromNil compileFn ast state =
    compileFn ast { state with Code = []; CodeLength = 0 }

let private combineStates (secondary: State) primary =
    { primary with
        Code = secondary.Code @ primary.Code
        CodeLength = primary.CodeLength + secondary.CodeLength
        SymbolsCount = secondary.SymbolsCount
        SymbolTable = secondary.SymbolTable }

let private lookupName name (state: State) =
    let rec loop depth symtab =
        match symtab with
        | [] -> None
        | first :: rest ->
            match Map.tryFind name first with
            | Some i -> Some(i, depth)
            | None -> loop (depth + 1) rest

    loop 0 state.SymbolTable

let inline private raiseException state format =
    sprintf format >> (+) $"({state.UnitName})" >> SyntaxException >> raise

let inline private raiseUndefined state = raiseException state "%s: 变量未定义."
let inline private raiseExported state = raiseException state "%s: 变量已导出."
let inline private raiseNotInLoop state = raiseException state "%s: 不在循环内."

let private namealloc name state =
    let first :: rest = state.SymbolTable

    state.SymbolsCount,
    { state with
        SymbolTable = Map.add name state.SymbolsCount first :: rest
        SymbolsCount = state.SymbolsCount + 1 }

let private makeConditional test body orelse compileTestFn compileFn state =
    let body' = fromNil compileFn body state

    let state' =
        state
        |> compileTestFn test
        |> addCode [ PopJumpIfFalse(body'.CodeLength + 2) ]
        |> combineStates body'

    match orelse with
    | None -> state' |> addCode [ Nop ]
    | Some orelse ->
        let orelse' = fromNil compileFn orelse state
        state' |> addCode [ Jump(orelse'.CodeLength + 1) ] |> combineStates orelse'

let private makeAssignment compileFn target valueFn state =
    // 保持从左到右的求值顺序.
    let before, after =
        match target with
        | NameTarget name ->
            match lookupName name state with
            | Some(slot, frame) -> id, addCode [ StoreVar(slot, frame) ]
            | None -> raiseUndefined state name
        | AttributeTarget(value', attr) -> compileFn value', addCode [ StoreAttr attr ]
        | SubscriptTarget(value', indexes) ->
            let before = compileFn value' >> Util.foldStateLast compileFn indexes
            before, addCode [ StoreSubscr(List.length indexes) ]

    state |> before |> valueFn |> after

let private makeFunction (compileFn: 'a -> State -> State) name parameters body (state: State) =
    let symbols = Seq.zip parameters (Seq.initInfinite id) |> Map.ofSeq

    let state' =
        { initState with
            SymbolTable = symbols :: state.SymbolTable
            SymbolsCount = List.length parameters
            UnitName = Option.defaultValue "匿名函数" name }

    let finalState = compileFn body state'

    let code =
        match finalState.Code with
        | ReturnValue :: _ -> finalState.Code
        | _ -> ReturnValue :: LoadConst null :: finalState.Code

    let instArgs =
        name, List.length parameters, finalState.SymbolsCount, code |> List.rev |> List.toArray

    addCode [ MakeFunction instArgs ] state

let private makeLet compileFn items state =
    let folder item state =
        match item with
        | LetProperties(props, value) ->
            let propsFolder prop state =
                let slot, state = namealloc prop state
                state |> addCode [ Copy; LoadAttr prop; StoreVar(slot, 0); Pop ]

            state
            |> compileFn value
            |> Util.foldStateLast propsFolder props
            |> addCode [ Pop ]
        | LetValue(name, value) ->
            let slot, state = namealloc name state

            match value with
            | Some v -> state |> compileFn v |> addCode [ StoreVar(slot, 0); Pop ]
            | None -> state

    Util.fold folder state items

let private makeLoopBody compileFn body addJumpInst headVariation state =
    let body': State = fromNil compileFn body { state with IsInLoop = true }

    let mapping pos inst =
        match inst with
        | IncompleteBreak -> Jump(pos + 2)
        | InCompleteContinue -> Jump(pos + 1 - body'.CodeLength - headVariation)
        | x -> x

    let body' =
        { body' with
            Code = List.mapi mapping body'.Code }

    state
    |> addJumpInst (body'.CodeLength + 2)
    |> combineStates body'
    |> addCode [ Jump(-body'.CodeLength - headVariation) ]

let private withVariation compileFn state =
    let origLength = state.CodeLength
    let compiled = compileFn state
    compiled, compiled.CodeLength - origLength

let rec compileExpr ast state =
    let cstmts = Util.foldStateLast compileStatement

    match ast with
    | Constant value -> addCode [ LoadConst value ] state
    | Name id ->
        match lookupName id state with
        | Some(slot, frame) -> addCode [ LoadVar(slot, frame) ] state
        | None -> raiseUndefined state id
    | Attribute(value, attr) -> state |> compileExpr value |> addCode [ LoadAttr attr ]
    | Subscript(value, indexes) ->
        state
        |> compileExpr value
        |> Util.foldStateLast compileExpr indexes
        |> addCode [ LoadSubscr(List.length indexes) ]
    | Call(func, args) ->
        state
        |> compileExpr func
        |> Util.foldStateLast compileExpr args
        |> addCode [ FunctionCall(List.length args) ]
    | BinOp(left, op, right) ->
        state
        |> compileExpr left
        |> compileExpr right
        |> addCode [ BinaryOperation <| Operators.getBinary op ]
    | UnaryOp(op, operand) ->
        state
        |> compileExpr operand
        |> addCode [ UnaryOperation <| Operators.getUnary op ]
    | BoolOp(op, values) ->
        let inst =
            match op with
            | And -> JumpIfFalseOrPop
            | Or -> JumpIfTrueOrPop

        let nilState = { state with Code = []; CodeLength = 0 }
        let (Some(values, last)) = Util.(|ListLast|_|) values
        let compiled = List.map ((Util.flip compileExpr) nilState) values

        let totalLength =
            compiled |> List.sumBy (fun s -> s.CodeLength) |> (+) (List.length compiled - 1)

        let folder value (state, totalLength) =
            let length = totalLength - value.CodeLength
            state |> combineStates value |> addCode [ inst (length + 2) ], length - 1

        Util.fold folder (state, totalLength) compiled |> fst |> compileExpr last
    | IfExpr(test, body, orelse) -> makeConditional test body (Some orelse) compileExpr compileExpr state
    | Array elts ->
        state
        |> Util.foldStateLast compileExpr elts
        |> addCode [ MakeArray(List.length elts) ]
    | Object entries ->
        let folder entry state =
            match entry with
            | AttributeEntry(attr, value) -> state |> addCode [ LoadConst attr ] |> compileExpr value
            | IndexEntry(index, value) -> state |> compileExpr index |> compileExpr value
            | FunctionEntry(name, parameters, body) ->
                state
                |> addCode [ LoadConst name ]
                |> makeFunction cstmts (Some name) parameters body

        state
        |> Util.foldStateLast folder entries
        |> addCode [ MakeObject(List.length entries) ]
    | This -> addCode [ LoadThis ] state
    | Lambda(parameters, name, body) -> state |> makeFunction cstmts name parameters body
    | SimpleLambda(parameters, name, body) -> state |> makeFunction cstmts name parameters [ Return <| Some body ]
    | Assign(target, value) -> makeAssignment compileExpr target (compileExpr value) state
    | AugAssign(target, op, value) ->
        let valueFn =
            match target with
            | NameTarget name ->
                match lookupName name state with
                | Some(slot, frame) -> addCode [ LoadVar(slot, frame) ]
                | None -> id // makeAssignment函数会抛出异常.
            | AttributeTarget(value', attr) -> addCode [ Copy; LoadAttr attr ]
            | SubscriptTarget(value', indexes) ->
                Util.foldStateLast compileExpr indexes
                >> addCode [ Copy; LoadSubscr(List.length indexes) ]

        makeAssignment
            compileExpr
            target
            (valueFn
             >> compileExpr value
             >> addCode [ BinaryOperation(Operators.getBinary op) ])
            state

and compileStatement ast state =
    let cstmts = Util.foldStateLast compileStatement

    match ast with
    | Expr value -> state |> compileExpr value |> addCode [ Pop ]
    | If(test, body, orelse) -> makeConditional test body orelse compileExpr cstmts state
    | FunctionDef(name, parameters, body) ->
        let slot, state = namealloc name state

        state
        |> makeFunction cstmts (Some name) parameters body
        |> addCode [ StoreVar(slot, 0) ]
    | Let items -> makeLet compileExpr items state
    | Return value ->
        match value with
        | Some v -> state |> compileExpr v |> addCode [ ReturnValue ]
        | None -> state |> addCode [ LoadConst null; ReturnValue ]
    | While(test, body) ->
        match test with
        | Constant c when c = true -> state |> makeLoopBody cstmts body (fun _ state -> state) 0
        | _ ->
            let state, headVariation = withVariation (compileExpr test) state

            state
            |> makeLoopBody cstmts body (addCode << List.singleton << PopJumpIfFalse) (headVariation + 1)
    | For(init, test, step, body) ->
        let state =
            match init with
            | Some(ExprInit expr) -> compileExpr expr state
            | Some(LetInit items) -> makeLet compileExpr items state
            | None -> state

        let state, headVariation =
            match step with
            | Some step ->
                let step' = fromNil compileExpr step state

                state
                |> addCode [ Jump(step'.CodeLength + 2) ]
                |> combineStates step'
                |> addCode [ Pop ],
                step'.CodeLength + 1
            | None -> state, 0

        let state, jumpInst, headVariation =
            match test with
            | None -> state, (fun _ state -> state), headVariation
            | Some test ->
                let state, testVariation = withVariation (compileExpr test) state
                state, (addCode << List.singleton << PopJumpIfFalse), headVariation + testVariation + 1

        state |> makeLoopBody cstmts body jumpInst headVariation
    | ForEach(target, iter, body) ->
        let slot, state, frame =
            match lookupName target state with
            | Some(slot, frame) -> slot, state, frame
            | _ -> Util.flattenDoublePair (namealloc target state, 0)

        state
        |> compileExpr iter
        |> addCode [ GetIter ]
        |> makeLoopBody cstmts body (fun delta -> addCode [ ForIter(delta + 2); StoreVar(slot, frame); Pop ]) 3
    | Try(body, handler) ->
        let body' = fromNil cstmts body state

        let state =
            state
            |> addCode [ PushTryContext <| body'.CodeLength + 2 ]
            |> combineStates body'

        match handler with
        | None -> addCode [ Jump 2; Pop ] state
        | Some(Handler(name, body)) ->

            match name with
            | Some name ->
                let slot, state = namealloc name state
                let handlerBody = fromNil cstmts body state

                state
                |> addCode [ Jump <| handlerBody.CodeLength + 3; StoreVar(slot, 0); Pop ]
                |> combineStates handlerBody
            | None ->
                let handlerBody = fromNil cstmts body state

                state
                |> addCode [ Jump <| handlerBody.CodeLength + 2; Pop ]
                |> combineStates handlerBody
    | Throw exc -> state |> compileExpr exc |> addCode [ ThrowException ]
    | Pass -> state
    | Break ->
        if state.IsInLoop then
            addCode [ IncompleteBreak ] state
        else
            raiseNotInLoop state "跳出"
    | Continue ->
        if state.IsInLoop then
            addCode [ InCompleteContinue ] state
        else
            raiseNotInLoop state "继续"

let compileTopLevel ast (state, exports) =
    match ast with
    | Statement stmt -> compileStatement stmt state, exports
    | Import(target, asname, path) ->
        let state = addCode [ ImportModule path ] state

        let state =
            match asname with
            | Some name ->
                let slot, state = namealloc name state
                state |> addCode [ StoreVar(slot, 0) ]
            | None -> state

        let state =
            match target with
            | ImportAll -> state
            | ImportNames names ->
                let folder name state =
                    let slot, state = namealloc name state
                    state |> addCode [ Copy; LoadAttr name; StoreVar(slot, 0); Pop ]

                Util.fold folder state names

        addCode [ Pop ] state, exports
    | Export items ->
        let folder (ExportItem(name, value)) (state, exports) =
            if Set.contains name exports then
                raiseExported state name
            else
                match value, lookupName name state with
                | None, Some _ -> state, Set.add name exports
                | Some value, _ ->
                    let slot, state = namealloc name state
                    let state = state |> compileExpr value |> addCode [ StoreVar(slot, 0); Pop ]
                    state, Set.add name exports
                | None, None -> raiseUndefined state name

        Util.fold folder (state, exports) items
    | ExportFunction(name, parameters, body) ->
        if Set.contains name exports then
            raiseExported state name
        else
            let slot, state = namealloc name state

            let state =
                state
                |> makeFunction (Util.foldStateLast compileStatement) (Some name) parameters body
                |> addCode [ StoreVar(slot, 0); Pop ]

            state, Set.add name exports

let compileModule symtab symcnt ast =
    let (Module body) = ast
    // 给代码加上返回指令.
    let appendReturn body =
        body @ [ Constant null |> Some |> Return |> Statement ]

    let body =
        match body with
        | Util.ListLast(init, last) ->
            match last with
            // 如果最后一个语句是表达式的话, 直接返回表达式, 方便repl使用.
            | Statement(Expr expr) -> init @ [ expr |> Some |> Return |> Statement ]
            | Statement(Return _) -> body
            | _ -> appendReturn body
        | _ -> appendReturn body

    let state, exports =
        Util.fold
            compileTopLevel
            ({ initState with
                SymbolTable = symtab
                SymbolsCount = symcnt },
             Set.empty)
            body

    { Code = state.Code |> List.rev |> List.toArray
      Symbols = state.SymbolTable.Head
      SymbolsCount = state.SymbolsCount
      Exports = exports }

let tryCompileModule symtab symcnt ast =
    try
        compileModule symtab symcnt ast |> Ok
    with SyntaxException msg ->
        Error msg
