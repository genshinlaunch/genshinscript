module Genshin.Lang.Evaluator

open System
open System.Collections

open DynamicService
open Dynamitey

open Genshin

open Compiler
open Exceptions
open Interop

type Return =
    | ToState of State
    | ToForeignCaller
    | ToEnvironment

and StackFrame =
    { Name: string
      Code: Instruction[]
      Locals: obj[]
      This: obj
      Return: Return }

and State =
    { Pos: int
      DataStack: obj list
      CallStack: StackFrame list
      TryContext: int list }

    member this.Name = this.CallStack.Head.Name
    member this.Code = this.CallStack.Head.Code

let initState =
    { Pos = 0
      DataStack = []
      CallStack = []
      TryContext = [] }

[<StructuredFormatDisplay("{Repr}")>]
type Function =
    { Name: string option
      ParamsCount: int
      LocalsCount: int
      Code: Instruction[]
      Closure: StackFrame list
      This: obj }

    override self.ToString() =
        match self.Name with
        | None -> "匿名函数"
        | Some name -> $"<函数 {name}>"

    member self.Repr = self.ToString()

[<AllowNullLiteral>]
type Object private (table: Hashtable) =
    inherit DynamicHashtable(table)

    let getPrototype () =
        let folder key state =
            match state with
            | None ->
                if table.ContainsKey(key) then
                    match table[key] with
                    | :? Object as proto -> Some proto
                    | _ -> None
                else
                    None
            | Some _ -> state

        Util.fold folder None [ "prototype"; "原型" ]

    new() = Object(Hashtable())
    new(d: IDictionary) = Object(Hashtable(d))
    new(d: IDictionary, equalityComparer: IEqualityComparer) = Object(Hashtable(d, equalityComparer))
    new(d: IDictionary, loadFactor: float32) = Object(Hashtable(d, loadFactor))

    new(d: IDictionary, loadFactor: float32, equalityComparer: IEqualityComparer) =
        Object(Hashtable(d, loadFactor, equalityComparer))

    new(equalityComparer: IEqualityComparer) = Object(Hashtable(equalityComparer))
    new(capacity: int) = Object(Hashtable(capacity))
    new(capacity: int, equalityComparer: IEqualityComparer) = Object(Hashtable(capacity, equalityComparer))
    new(capacity: int, loadFactor: float32) = Object(Hashtable(capacity, loadFactor))

    new(capacity: int, loadFactor: float32, equalityComparer: IEqualityComparer) =
        Object(Hashtable(capacity, loadFactor, equalityComparer))

    member private _.TryItemPrivate(thisToBind, key) : obj option =
        if table.ContainsKey(key) then
            match table[key] with
            | :? Function as func -> Some <| { func with This = thisToBind }
            | value -> Some value
        else
            match getPrototype () with
            | Some proto -> proto.TryItemPrivate(thisToBind, key)
            | None -> None

    override this.TryItem(key) = this.TryItemPrivate(this, key)

    override this.Clone() =
        Object(this.Table.Clone() :?> Hashtable)

// 用类和接口封装可变数据.
type IModulesManager =
    abstract LoadModule: string -> obj

let private extractArgsFromList argsCount dataStack =
    let rec loop argsCount dataStack acc =
        match argsCount, dataStack with
        | 0, _ -> acc, dataStack
        | _, x :: xs -> loop (argsCount - 1) xs (x :: acc)

    loop argsCount dataStack []

let private extractArgs argsCount state =
    extractArgsFromList argsCount state.DataStack

let private throwException recurse exc (state: State) =
    let rec loop trace state =
        match state with
        | { TryContext = exit :: rest } ->
            recurse
                { state with
                    Pos = exit
                    TryContext = rest
                    DataStack = exc :: state.DataStack }
        | { CallStack = { Return = ToState next; Name = name } :: _ } -> loop (name :: trace) next
        | _ ->
            match state.CallStack.Head.Return with
            | ToForeignCaller -> raise <| GenshinException(exc, trace)
            | ToEnvironment ->
                eprintfn "未捕获的异常: %O" exc
                trace |> List.iter (eprintfn "    来自 %s")
                null

    loop [ state.Name ] state

let private makeJumpOrPop loop pred delta state =
    let tos :: rest = state.DataStack

    if pred tos then
        loop { state with Pos = state.Pos + delta }
    else
        loop
            { state with
                Pos = state.Pos + 1
                DataStack = rest }

let inline private arityMismatch func argsCount =
    $"{func}: 参数不匹配. 函数接受{func.ParamsCount}个参数, 但得到了{argsCount}个."

let eval (modulesManager: IModulesManager) code callStack =
    let rec inline tryLoop delayedState state =
        let forcedState =
            try
                delayedState () |> Ok
            with exc ->
                Error exc

        match forcedState with
        | Ok forcedState -> loop forcedState
        | Error exc -> throwException loop exc state

    and loop state =
        let moved = { state with Pos = state.Pos + 1 }

        assert (state.Pos < state.Code.Length)

        match state.Code[state.Pos] with
        | LoadConst v ->
            loop
                { moved with
                    DataStack = v :: state.DataStack }
        | Copy ->
            loop
                { moved with
                    DataStack = state.DataStack.Head :: state.DataStack }
        | BinaryOperation op ->
            let y :: x :: rest = state.DataStack

            tryLoop
                (fun () ->
                    { moved with
                        DataStack = OperatorInvoker.InvokeBinary(x, op, y) :: rest })
                state
        | UnaryOperation op ->
            let x :: rest = state.DataStack

            tryLoop
                (fun () ->
                    { moved with
                        DataStack = OperatorInvoker.InvokeUnary(op, x) :: rest })
                state
        | GetIter ->
            match state.DataStack with
            | :? IEnumerable as iterable :: rest ->
                tryLoop
                    (fun () ->
                        { moved with
                            DataStack = iterable.GetEnumerator() :: rest })
                    state
            | first :: _ -> invalidOp $"类型{first.GetType()}不可迭代."
        | ForIter delta ->
            match state.DataStack with
            | :? IEnumerator as iter :: _ ->
                try
                    if iter.MoveNext() then
                        loop
                            { moved with
                                DataStack = iter.Current :: state.DataStack }
                    else
                        loop { state with Pos = state.Pos + delta }
                with exc ->
                    throwException loop exc state
            | first :: rest -> throwException loop $"类型{first.GetType()}不是迭代器." state
        | ReturnValue ->
            match state with
            | { CallStack = { Return = ToState next } :: _
                DataStack = tos :: _ } ->
                loop
                    { next with
                        DataStack = tos :: next.DataStack }
            | { DataStack = tos :: _ } -> tos
            | _ -> null
        | FunctionCall argsCount ->
            let args, func :: stack = extractArgs argsCount state

            match func with
            | :? Function as func ->
                if argsCount <> func.ParamsCount then
                    throwException loop (arityMismatch func argsCount) state
                else
                    let makeFrame locals ``return`` =
                        { Name = string func
                          Locals = locals
                          Code = func.Code
                          This = func.This
                          Return = ``return`` }

                    let tos :: _ = state.CallStack
                    let tail = state.Code[moved.Pos] = ReturnValue

                    let locals =
                        if tos.Locals.Length = func.ParamsCount && tail then
                            tos.Locals
                        else
                            Array.zeroCreate func.LocalsCount

                    List.iteri (fun i arg -> locals[i] <- arg) args

                    if tail then
                        loop
                            { initState with
                                CallStack = makeFrame locals tos.Return :: func.Closure }
                    else
                        loop
                            { initState with
                                CallStack = makeFrame locals (ToState { moved with DataStack = stack }) :: func.Closure }
            | _ ->
                tryLoop
                    (fun () ->
                        { moved with
                            DataStack = Dynamic.Invoke(func, args = Array.ofList args) :: stack })
                    state
        | ImportModule path ->
            tryLoop
                (fun () ->
                    { moved with
                        DataStack = modulesManager.LoadModule(path) :: state.DataStack })
                state
        | Jump delta -> loop { state with Pos = state.Pos + delta }
        | JumpIfFalseOrPop delta -> makeJumpOrPop loop Util.isFalse delta state
        | JumpIfTrueOrPop delta -> makeJumpOrPop loop (not << Util.isFalse) delta state
        | LoadSubscr indexesCount ->
            let indexes, subject :: rest = extractArgs indexesCount state

            tryLoop
                (fun () ->
                    { moved with
                        DataStack = Dynamic.InvokeGetIndex(subject, indexes = Array.ofList indexes) :: rest })
                state
        | LoadAttr attr ->
            let subject :: rest = state.DataStack

            if isNull subject then
                throwException loop (NullReferenceException()) state
            else
                match getInstanceMember subject attr with
                | Some mem -> loop { moved with DataStack = mem :: rest }
                | None ->
                    tryLoop
                        (fun () ->
                            { moved with
                                DataStack = Dynamic.InvokeGet(subject, attr) :: rest })
                        state
        | LoadThis ->
            loop
                { moved with
                    DataStack = state.CallStack.Head.This :: state.DataStack }
        | LoadVar(slot, frame) ->
            let value = (state.CallStack |> List.item frame).Locals |> Array.item slot

            loop
                { moved with
                    DataStack = value :: state.DataStack }
        | MakeArray eltsCount ->
            let array = ArrayList()

            let rec buildArray dataStack eltsCount =
                match dataStack, eltsCount with
                | _, 0 -> dataStack
                | x :: xs, _ ->
                    array.Add(x) |> ignore
                    buildArray xs (eltsCount - 1)

            let rest = buildArray state.DataStack eltsCount
            array.Reverse()

            loop { moved with DataStack = array :: rest }
        | MakeFunction(name, paramsCount, localsCount, code) ->
            let func =
                { Name = name
                  ParamsCount = paramsCount
                  LocalsCount = localsCount
                  Code = code
                  Closure = state.CallStack
                  This = null }

            loop
                { moved with
                    DataStack = func :: state.DataStack }
        | MakeObject entriesCount ->
            let object = Object()

            let rec buildObject entriesCount dataStack =
                match entriesCount, dataStack with
                | 0, _ -> dataStack
                | _, value :: key :: rest ->
                    object[key] <- value
                    buildObject (entriesCount - 1) rest

            let stack = buildObject entriesCount state.DataStack

            loop
                { moved with
                    DataStack = object :: stack }
        | Nop -> loop moved
        | Pop ->
            loop
                { moved with
                    DataStack = state.DataStack.Tail }
        | PopJumpIfFalse delta ->
            let tos :: rest = state.DataStack
            let newPos = if Util.isFalse tos then state.Pos + delta else moved.Pos

            loop
                { state with
                    Pos = newPos
                    DataStack = rest }
        | PushTryContext handler ->
            loop
                { moved with
                    TryContext = handler + state.Pos :: state.TryContext }
        | StoreSubscr indexesCount ->
            let value :: rest = state.DataStack
            let indexes, subject :: rest = extractArgsFromList indexesCount rest
            let indexesThenValue = Array.concat [ Array.ofList indexes; [| value |] ]

            tryLoop
                (fun () ->
                    Dynamic.InvokeSetIndex(subject, indexesThenValue = indexesThenValue) |> ignore

                    { moved with DataStack = value :: rest })
                state
        | StoreAttr attr ->
            let value :: subject :: rest = state.DataStack

            tryLoop
                (fun () ->
                    Dynamic.InvokeSet(subject, attr, value) |> ignore

                    { moved with DataStack = value :: rest })
                state
        | StoreVar(slot, frame) ->
            let value :: rest = state.DataStack
            (state.CallStack |> List.item frame).Locals |> (fun arr -> arr[slot] <- value)

            loop { moved with DataStack = value :: rest }
        | ThrowException ->
            let exc :: rest = state.DataStack
            throwException loop exc { state with DataStack = rest }
        | IncompleteBreak
        | InCompleteContinue -> failwith "编译器异常" // 这不应发生.

    let callStack =
        [ for i in callStack ->
              { Name = "模块"
                Locals = i
                Code = code
                This = null
                Return = ToEnvironment } ]

    loop { initState with CallStack = callStack }

let private dummyModulesManager =
    { new IModulesManager with
        member _.LoadModule(_) = null } // 函数内部不会导入模块.

type Function with

    member self.Invoke(args: obj[]) =
        if args.Length <> self.ParamsCount then
            invalidOp <| arityMismatch self args.Length

        let locals = Array.zeroCreate self.LocalsCount
        Array.iteri (fun i arg -> locals[i] <- arg) args
        eval dummyModulesManager self.Code (locals :: [ for i in self.Closure -> i.Locals ])
