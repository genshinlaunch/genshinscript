﻿namespace Genshin

open Argu

open Genshin.Lang
open Genshin.Pervasive

open Environment
open Exceptions

[<HelpFlags("-h", "--help")>]
type CliArguments =
    | [<MainCommand; ExactlyOnce; Last>] File of file: string

    interface IArgParserTemplate with
        member this.Usage =
            match this with
            | File(_) -> "program read from script file"

module Program =
    [<EntryPoint>]
    let main args =
        let parser = ArgumentParser.Create<CliArguments>("GenshinScript.exe")
        // Argu包其实只用于打印帮助信息, 因为目前命令行选项还不多.
        // 应用完善后我会好好利用Argu的.

        match args with
        | [| "-h" | "--help" |] -> printfn "%s" <| parser.PrintUsage()
        | [||] ->
            let banner =
                """
源深琦东(R) 源语言交互窗口版本 GenshinScript 0.9.13 的 1.0
版权所有(C) 源深琦东工作室. 保留所有权利.

若要获得帮助, 请键入#help;; / #帮助；；
"""

            Repl.interact banner
        | [| path |] ->
            let modulesManager =
                ModulesManager(Default.pervasive, System.Environment.CurrentDirectory)

            let env = Environment(modulesManager, Default.pervasive)
            env.ExecuteFile(path) |> ignore
        | _ -> raise <| InvalidCommandlineOptionsException "无效命令行选项"

        0
